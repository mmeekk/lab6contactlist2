package th.ac.tu.siit.lab6contactlist;
import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.*;
import android.widget.*;

public class AddNewActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_new);
		//
		Intent data = this.getIntent();
		if(data.hasExtra("pos")){
			String name = data.getStringExtra("name");
			String phone  = data.getStringExtra("phone");
			int sType = Integer.parseInt(data.getStringExtra("type"));
			String email = data.getStringExtra("email");
			
			
			EditText etName = (EditText)findViewById(R.id.etName);
			EditText etPhone = (EditText)findViewById(R.id.etPhone);
			RadioGroup rdgType = (RadioGroup)findViewById(R.id.rdgType);
			EditText etEmail = (EditText)findViewById(R.id.etEmail);
			
			etName.setText(name);
			etPhone.setText(phone);
			etEmail.setText(email);
			if (sType == R.drawable.home) {
				RadioButton rdHome = (RadioButton)findViewById(R.id.rdHome);
				rdHome.setChecked(true);
			}
			else{
				RadioButton rdMobile = (RadioButton)findViewById(R.id.rdMobile);
				rdMobile.setChecked(true);
			}
			
		}
	}	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_new, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_save) {
			returnRecord();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void returnRecord() {
		EditText etName = (EditText)findViewById(R.id.etName);
		EditText etPhone = (EditText)findViewById(R.id.etPhone);
		RadioGroup rdgType = (RadioGroup)findViewById(R.id.rdgType);
		EditText etEmail = (EditText)findViewById(R.id.etEmail);
		
		String sName = etName.getText().toString().trim();
		String sPhone = etPhone.getText().toString().trim();
		String sEmail = etEmail.getText().toString().trim();
		int iType = rdgType.getCheckedRadioButtonId();
		
		if (sName.length() == 0 || sPhone.length() == 0 || iType == -1) {
			AlertDialog dialog = new AlertDialog.Builder(this).create();
			dialog.setTitle("Error");
			dialog.setMessage("All fields are required.");
			dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK", 
				new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) 
				{}});
			dialog.show();
		}
		else {
			Intent data = new Intent();
			data.putExtra("name", sName);
			data.putExtra("phone", sPhone);
			data.putExtra("email", sEmail);
			data.putExtra("pos", data.getIntExtra("pos", 0));
			String sType = "";
			switch(iType) {
			case R.id.rdHome:
				sType = "home";
				break;
			case R.id.rdMobile:
				sType = "mobile";
				break;
			}
			data.putExtra("type", sType);
			this.setResult(RESULT_OK, data);
			this.finish();
			Toast t = Toast.makeText(this, "A new contact added.", 
					Toast.LENGTH_LONG);
			t.show();
		}
	}
	
}
